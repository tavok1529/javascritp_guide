(() => {

    class Persona {
        static _conteo = 0;
        static get conteo() {
            return Persona._conteo + ' instances..';
        }

        static mensaje (){
            console.log(this.nombre); // esto da undefinend
            console.log("Soy un metodo statico..");
        }



        nombre = '';
        codigo = '';
        frase = '';
        comida = '';

        /**
         * El constructor es el unuco metod que retorna 
         * una instancia de un metodo
         */
        constructor(nombre = 'S/V', codigo = "S/V", frase = "S/V") {
            this.nombre = nombre;
            this.codigo = codigo;
            this.frase = frase;
            Persona._conteo++;
        }

        quienSoy() {
            console.log('Soy ' + this.nombre + ' mi identidad es: ' + this.codigo);
        }
        miFrase() {
            this.quienSoy();
            console.log(this.codigo + ' ' + this.frase)
        }

        set setComidaFavorita(comida) {
            this.comida = comida.toUpperCase()
        }
        get getComidaFavorita() {
            return 'La comida favorita de ' + this.nombre + " es " + this.comida;
        }

    }


    const spiderman = new Persona('Peter parker', 'Spiderman', 'Soy tu vecino spiderman');
    const ironman = new Persona('Tony stark', 'Ironman', 'Ironman');
    console.log(spiderman);
    console.log(ironman);


    spiderman.quienSoy();
    ironman.quienSoy();
    spiderman.miFrase()
    ironman.miFrase();

    // utilizando el set
    spiderman.setComidaFavorita = 'Pay de cereza de la tia may';
    spiderman.nemesis = 'Duende verde';

    //se puede reemplazar el contentido de un
    //valor
    //spiderman.comida = 'Duende verde';

    console.log(spiderman.getComidaFavorita);
    console.log(spiderman);
    console.log("Conteo statico: " + Persona._conteo);
    console.log(Persona.conteo);
    Persona.mensaje();




})()