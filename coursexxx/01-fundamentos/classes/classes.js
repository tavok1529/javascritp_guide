(() => {
    //estos son dos ojetos diferentes
    const fher = {
        nombre: "Octavio",
        edad: 30,
        imprimir() {
            console.log('Nombre : ' + this.nombre + ' - edad: ' + this.edad);
        }
    }
    const pedro = {
        nombre: 'pedor',
        edad: 15
    }

    fher.imprimir();

    // cuando una funcion inia con mayus es para crear instancias
    // este se hacia anteriormente
    // y se debia documentar indicando que se debe usar la palabra new
    function Persona(nombre, edad) {
        console.log('Se ejecuto esta linea');
        this.nombre = nombre;
        this.edad = edad;
        this.imprimir = () => {
            console.log('Nombre : ' + this.nombre + ' - edad: ' + this.edad);
        }
    }

    const maria = Persona('Maria', 23);
    console.log(maria)
    // para crear una nueva instancia usamos la palabra
    // new  por ejemplo

    const maria2 = new Persona('Maria', 23);
    console.log(maria2)
    maria2.imprimir();

    const melisa = new Persona('Melisa', 23);
    console.log(melisa)
    melisa.imprimir();




})()