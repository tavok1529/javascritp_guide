(() => {

    class Persona {

        static porObecto({ nombre, apellido, pais }) {
            return new Persona(nombre, apellido, pais);
        }

        constructor(nombre, apellido, pais) {
            this.nombre = nombre;
            this.apellido = apellido;
            this.pais = pais;
        }

        getInfo() {
            console.log("info: " + this.nombre + " " + this.apellido + " " + this.pais);
        }
    }
    const nombre1 = 'meliisa',
        apellido1 = 'Flores',
        pais1 = 'honduras';

    const fher = {
        nombre: 'fernando',
        apellido: 'Herrera',
        pais: 'Costa rica'
    }
    const persona1 = new Persona(nombre1, apellido1, pais1);
    const persona2 = Persona.porObecto(fher);

    persona1.getInfo();
    persona2.getInfo();




})()