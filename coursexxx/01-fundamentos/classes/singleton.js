(() => {

    class Singleton {
        static instancia; //undefined ? 
        nombre = '';

        constructor(nombre = '') {
            if (!!Singleton.instancia) {
                return Singleton.instancia
            }
            Singleton.instancia = this;
            this.nombre = nombre;
        }

    }


    const instance1 = new Singleton('Ironaman');
    const instance2 = new Singleton('Spiderman');
    const instance3 = new Singleton('Blacpanter');

    console.log("Nombre en la instancia1 es: " + instance1.nombre)
    console.log("Nombre en la instancia2 es: " + instance2.nombre)
    console.log("Nombre en la instancia3 es: " + instance3.nombre)



})()