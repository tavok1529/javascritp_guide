import { buscarHeroe, buscarHeroeAsync } from './js/promesas';

// promesaLenta.then(console.log);
// promesaMedia.then(console.log);
// promesaRapida.then(console.log);


/**
 * Promise.race nos permite ejecutar todas las promesas
 * en conjunto y obtener le mensaje de la promesa
 * que se resuelve mas rapido asi falle todas la que se
 * resuelva primero con o sin error es la que
 * se va a ejecutar
 */

// Promise.race([promesaLenta, promesaMedia, promesaRapida])
//     // .then(mensaje => console.log(mensaje));
//     .then(console.log)
//     .catch(console.warn);

buscarHeroe('capi2')
    .then(heroe => console.log(heroe))
    .catch(console.warn);

buscarHeroeAsync('iron2')
    .then(heroe => console.log(heroe))
    .catch(console.warn);