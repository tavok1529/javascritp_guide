import { obtenerHeroesArr } from './js/await';

// const heores = obtenerHeroesArr();

// console.table(heores);
console.time('await');
// obtenerHeroesArr().then(console.table);
obtenerHeroesArr().then(heroes => {
    console.table(heroes);
    console.timeEnd('await');
});
