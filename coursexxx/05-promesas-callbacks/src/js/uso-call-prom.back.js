import { buscarHeroe as buscarHerroCallback } from './js/callbacks';
import { buscarHeroe } from './js/promesas';
import './styles.css';

const heroeId1 = 'capi';
const heroeId2 = 'iron';



// buscarHeroe(heroeId1).then(hero1 => {
//     //console.log(`Enviando a ${hero.nombre} a la mision`);
//     buscarHeroe(heroeId2).then(hero2 => {
//         console.log(`enviando a ${hero1.nombre} y ${hero2.nombre} a la mision`);
//     });
// });


//promesas en paralelo
Promise.all([true, 'Hola', 123]).then(arr => {
    console.log('promise.all', arr)
});

// se aplica la desestructuraciond de array
Promise.all([buscarHeroe(heroeId1), buscarHeroe(heroeId2)])
    .then(([hero1, hero2]) => {
        // console.log(`enviando a ${heroes[0].nombre} y ${heroes[1].nombre} a la mision`);
        console.log(`enviando a ${hero1.nombre} y ${hero2.nombre} a la mision`);
    }).catch(err => {
        alert(err)
    }).finally(() => {
        console.log('Se termino el promise.all')
    });


console.log('Fin del programa')
