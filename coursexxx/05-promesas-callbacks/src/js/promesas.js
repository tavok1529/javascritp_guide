const heroes = {
    capi: {
        nombre: 'Capitan america',
        poder: "aguantar inyecciones"
    },
    iron: {
        nombre: 'Ironamn',
        poder: "mucho billete "
    },
    spider: {
        nombre: 'Spiderman',
        poder: "Aguantar picaduras de arañas"
    },
}



//sin el async
export const buscarHeroe = (id) => {
    const hero = heroes[id];
    //si lo vamos a usar como variable
    //const promesa = new Promise();
    //en este caso solo vamos a retornar la promesa
    // se usan los parametros resolve y reject
    return new Promise((resolve, reject) => {
        if (hero) {
            setTimeout(() => resolve(hero), 1000);
        } else {
            reject(`No existe un heroe con el id ${id}`);
        }
    });
}



export const buscarHeroeAsync = async (id) => {
    const hero = heroes[id];
    //si lo vamos a usar como variable
    //const promesa = new Promise();
    //en este caso solo vamos a retornar la promesa
    // se usan los parametros resolve y reject
    if (hero) {
        return hero;
    } else {
        //  para tener mas traza en si e sun erro inesperado
        // throw Error(`No existe un heroe con el id ${id}`);
        throw `No existe un heroe con el id ${id}`;
    }

}

export {
    promesaLenta,
    promesaMedia,
    promesaRapida
}

const promesaLenta = new Promise((resolve, reject) => {
    setTimeout(() => resolve('promesa Lenta'), 2000);
});
const promesaMedia = new Promise((resolve, reject) => {
    setTimeout(() => resolve('promesa Media'), 1500);
});
const promesaRapida = new Promise((resolve, reject) => {
    setTimeout(() => resolve('promesa Rapida'), 1000);
});