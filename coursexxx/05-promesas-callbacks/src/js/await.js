import { buscarHeroeAsync, buscarHeroe } from "./promesas";

const heroesId = ['capi', 'iron', 'spider'];

export const obtenerHeroesArr = async () => {
    // const heroesArr = [];
    // for (const id of heroesId) {
    //     // buscarHeroeAsync(id)
    //     //     .then(hero => heroesArr.push(hero));
    //     // const heroe = await buscarHeroe(id);
    //     heroesArr.push(buscarHeroe(id));
    // }
    // // setTimeout(() => {
    // //     console.table(heroesArr)
    // // }, 1000);
    // // return heroesArr;
    // return await Promise.all(heroesArr);

    // forma mas elegante 
    return await Promise.all(heroesId.map(buscarHeroe));
};